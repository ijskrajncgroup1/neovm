import string
####################################################################################################
####################################MISCELLANEOUS QUERIES###########################################
####################################################################################################
# This function sets up a database from the input script
def setupDB(neo4j,script):
    neo4j.run("MATCH (n) DETACH DELETE n")
    neo4j.run(script)
# This function checks if any of the polygons adjacent to the input edge is a triangle
def count_edge_triangles(EID:int, neo4j):
    e_polys=string.Template("""MATCH (e:Edge{name:$EdgeID})
    MATCH (e)-[:IS_PART_OF]->(p:Polygon)
    WITH p
    MATCH (p)<-[:IS_PART_OF]-(e:Edge)
    WITH p, count(e) AS cnt WHERE cnt=3
    RETURN count(1) AS nrtri""")
    return neo4j.run(e_polys.substitute(EdgeID=str(EID))).data()[0]['nrtri']
# This function returns id of a triangle adjacent to the input edge
def edge_triangle(EID:int, neo4j):
    q=string.Template("""MATCH (e:Edge{name:$EdgeID})
    MATCH (e)-[:IS_PART_OF]->(p:Polygon)
    WITH p
    MATCH (p)<-[:IS_PART_OF]-(e:Edge)
    WITH p, count(e) AS cnt WHERE cnt=3
    RETURN p.name AS pID""")
    return neo4j.run(q.substitute(EdgeID=str(EID))).data()[0]['pID']
# This function checks for triangles around triangles
def polygon_triangles(PID:int, neo4j):
    q=string.Template("""MATCH (p:Polygon{name:$PolyID})
    MATCH (p)<-[:IS_PART_OF]-(e:Edge)
    MATCH (e)-[:IS_PART_OF]->(op:Polygon) WHERE NOT op=p
    WITH DISTINCT op
    MATCH (e:Edge)-[:IS_PART_OF]->(op)
    WITH op, count(e) AS cnt WHERE cnt=3
    RETURN count(1) AS nrtri""")
    return neo4j.run(q.substitute(PolyID=str(PID))).data()[0]['nrtri']
# This function returns all 3 edges of the input triangle
def triangle_edges(pID:int, neo4j):
    q=string.Template("""MATCH (p:Polygon{name:$PolyID})
    MATCH (e:Edge)-[:IS_PART_OF]->(p)
    RETURN collect(e.name) AS edges""")
    return neo4j.run(q.substitute(PolyID=str(pID))).data()[0]['edges']
# This function counts common polygons between cells c4 and c5
def count_shared_polygons_c4c5(EID:int, neo4j):
    q=string.Template("""MATCH (e7:Edge{name:$EdgeID})-[:IS_PART_OF]->(p:Polygon)-[:IS_PART_OF]-(c:Cell)
    WITH e7, Collect(Distinct c) As neicell
    MATCH (e7)<-[:IS_PART_OF]-(v:Vertex)-[:IS_PART_OF]->(e:Edge)-[:IS_PART_OF]->(p:Polygon)-[:IS_PART_OF]->(tcell:Cell) WHERE NOT tcell IN neicell
    WITH collect (DISTINCT tcell) As list
    WITH list[0] As c4, list[1] As c5
    MATCH (c4)<-[:IS_PART_OF]-(p:Polygon)-[:IS_PART_OF]->(c5)
    WITH DISTINCT p
    RETURN count(p) AS ppoly""") 
    return neo4j.run(q.substitute(EdgeID=str(EID))).data()[0]['ppoly']
# This function counts the number of free nodes of the input entity
def count_free_nodes(ent,neo4j):
    q=string.Template("""MATCH (n:Free)-[:CONTAINS]->(v:$entity)
    WITH collect(v) AS vlist
    RETURN size(vlist) as nr""")
    return neo4j.run(q.substitute(entity=ent)).data()[0]['nr']
# This function creates a new node in graph and marks it as free
def new_node(ent,entID:int,neo4j):
    q=string.Template("""CREATE (v:$entity {name:$eID})
    WITH v
    MATCH (nf:Free)
    CREATE (nf)-[:CONTAINS]->(v)""")
    neo4j.run(q.substitute(eID=str(entID),entity=ent))
# This function returns info on edges, polygons and cells that were involved in a T1 transition
def fetch_new_data(object1, object2, neo4j):
    q=string.Template("""MATCH (t1:T1)-[]->(n:$obj1)
    MATCH (m:$obj2)-[r:IS_PART_OF]->(n)
    WITH n, collect([m.name,r.ori]) AS dta
    RETURN collect([n.name,dta]) AS info""")
    return neo4j.run(q.substitute(obj1=str(object1),obj2=str(object2))).data()[0]['info']
# This function releases the helping nodes
def releaseHelpingNodes(neo4j):
    q1= "MATCH (nn:T1)-[r]-(n) DELETE r RETURN nn,n"
    # Deletes creation nodes
    q2= "MATCH (c:Creation) DETACH DELETE c"
    # Deletes deletion nodes
    q3= "MATCH (d:Deletion) DETACH DELETE d"
    for q in [q1,q2,q3]:
        neo4j.run(q)
# This function finds polygons p1, p2, and p3
def polygons_p1_p2_p3(neo4j):
    q="""OPTIONAL MATCH (n:T1)-[:p1]->(p1)
    OPTIONAL MATCH (n)-[:p2]->(p2)
    OPTIONAL MATCH (n)-[:p3]->(p3)
    RETURN COLLECT(p1.name) + COLLECT(p2.name) + COLLECT(p3.name) AS p1p2p3"""
    return neo4j.run(q).data()[0]['p1p2p3']
# This function finds vertices v1, v2
def vertices_v1_v2(neo4j):
    q="""MATCH (t1:T1)
    MATCH (v2)<-[:v2]-(t1)-[:v1]->(v1)
    WITH v1, v2
    RETURN [v1.name, v2.name] AS v1v2"""
    return neo4j.run(q).data()[0]['v1v2']
# This function finds vertices v1, v2, v3
def vertices_v1_v2_v3(neo4j):
    q="""OPTIONAL MATCH (t1:T1)-[:v1]->(v1)
    OPTIONAL MATCH (t1)-[:v2]->(v2)
    OPTIONAL MATCH (t1)-[:v3]->(v3)
    RETURN COLLECT(v1.name) + COLLECT(v2.name) + COLLECT(v3.name) AS v1v2v3"""
    return neo4j.run(q).data()[0]['v1v2v3']
# This function returns id of a particular entity
def find_node(ent,neo4j):
    q=string.Template("""MATCH (t1:T1)-[:$entity]->($entity)
    RETURN $entity.name AS $entity""")
    return neo4j.run(q.substitute(entity=ent)).data()[0][ent]
# This function frees a specific node
def free_node(ent, neo4j):
    q=string.Template("""MATCH (t1:T1)-[:$entity]->($entity)
    MATCH (nf:Free)
    CREATE (nf)-[:CONTAINS]->($entity)""")
    return neo4j.run(q.substitute(entity=ent))
###############################################




