import matplotlib.pyplot as plt
import numpy as np
import os

def plot_2D_snap(path, file_number):
    file_name = f"out_{file_number}.vt3d"
    file_path = os.path.join(path, file_name)
    with open(file_path, "r") as file:
        # Read the lines
        lines = file.readlines()
        # Extract values from the first line
        values_first_line = list(map(int, lines[0].split()))
        Nv, Ne, Np, Nc = values_first_line
        L=[]
        # Extract values from the third line
        values_third_line = list(map(float, lines[2].split()))
        Lx, Ly, Lz = values_third_line
        L.append(Lx)
        L.append(Ly)
        L.append(Lz)
        # Extract values from the fifth line
        values_fifth_line = list(map(float, lines[4].split()))
        flag, vx, vy, vz = values_fifth_line
        vertices=[]
        for i in range(4, 4 + Nv):
            list1=list(map(float, lines[i].split()))
            temp=[]
            temp.append(list1[1])
            temp.append(list1[2])
            temp.append(list1[3])
            vertices.append(temp)
        edges=[]
        for i in range(4 + Nv+1, 4 + Nv+Ne+1):
            list1=list(map(int, lines[i].split()))
            temp=[]
            temp.append(list1[1])
            temp.append(list1[2])
            edges.append(temp)
        polygons=[]
        for i in range(4 + Nv+Ne+1+1, 4 + Nv+Ne+1+1+Np):
            list1=list(map(int, lines[i].split()))
            temp=[]
            for i in range(len(list1)):
                temp.append(list1[i])
            polygons.append(temp)
    # Arranging edges of each polygon in anti-clockwise direction
    pp = []
    for j in range(len(polygons)):
        opolygons = [polygons[j][0]]  # Initialize opolygons[j]
        if polygons[j][0] == 1:
            p = polygons[j][1:]  # Drop the first element
            i = 0
            oeID = p[i]
            opolygons.append(oeID)
            if oeID > 0:
                v1, v2 = edges[abs(oeID) - 1][0], edges[abs(oeID) - 1][1]
            else:
                v1, v2 = edges[abs(oeID) - 1][1], edges[abs(oeID) - 1][0]
            for kk in range(len(p)-1):
                for k in range(len(p)):
                    if k != i:
                        oeID = p[k]
                        if oeID > 0:
                            V1N, V2N = edges[abs(oeID) - 1][0], edges[abs(oeID) - 1][1]
                        else:
                            V1N, V2N = edges[abs(oeID) - 1][1], edges[abs(oeID) - 1][0]
                        if v2 == V1N:
                            opolygons.append(oeID)
                            v2 = V2N
                            break
        pp.append(opolygons)
    polygons = pp
    # Collecting coordinates of each polygon
    cell = []
    for i in range(len(polygons)):
        pID = i
        vref = vertices[edges[abs(polygons[pID][1]) - 1][0] - 1]
        polyCOOR = []
        for j in range(1, len(polygons[pID])):
            oeID = polygons[pID][j]
            if oeID > 0:
                V1 = vertices[edges[abs(oeID) - 1][0] - 1]
                V2 = vertices[edges[abs(oeID) - 1][1] - 1]
            else:
                V1 = vertices[edges[abs(oeID) - 1][1] - 1]
                V2 = vertices[edges[abs(oeID) - 1][0] - 1]
            dxdydz = [0, 0, 0]
            for k in range(3):
                if abs(V1[k] - vref[k]) > 0.5 * L[k]:
                    if V1[k] > vref[k]:
                        dxdydz[k] -= L[k]
                    else:
                        dxdydz[k] += L[k]
            polyCOOR.append([V1[l] + dxdydz[l] for l in range(3)])
        cell.append(polyCOOR)

    # Define the periodic shifts
    delta = [
        [0, 0, 0],
        [1, 0, 0],
        [0, 1, 0],
        [1, 1, 0],
        [1, -1, 0],
        [0, -1, 0],
        [-1, 1, 0],
        [-1, 0, 0],
        [-1, -1, 0]
    ]
    disp_vec = [[component * L[i] for i, component in enumerate(sublist)] for sublist in delta]
    cells2plot=[]
    for disp in disp_vec:
        for apoly in cell:
            temp=[]
            for v in apoly:
                newv=[x + y for x, y in zip(v, disp)]
                temp.append(newv)
            cells2plot.append(temp)
    plt.clf() # clear the previous plot (if any)
    # Plotting each polygon seperately
    for polygon in cells2plot:
        polygon.append(polygon[0])  # Closing the loop by adding the first point to the end
        xs, ys, _ = zip(*polygon)  # Extract x and y coordinates
        # Plot the filled polygon with black boundary
        plt.fill(xs, ys, color='grey', edgecolor='black', alpha=0.5)
    # Set the x and y limits
    plt.xlim(0, L[0])
    plt.ylim(0, L[1])
    # Remove tick marks and labels
    plt.tick_params(axis='both', which='both', bottom=False, left=False, labelbottom=False, labelleft=False)
    # Set the aspect ratio to be equal
    plt.gca().set_aspect('equal', adjustable='box')
    # Adjust figure size to fit content tightly
    plt.tight_layout(pad=0)
    # Save the plot as a .pdf file
    out_file_name = f"out_{file_number}.pdf"
    out_file_path = os.path.join(path, out_file_name)
    plt.savefig(out_file_path, format='pdf', bbox_inches='tight', pad_inches=0)
    # Close the plot window
    plt.close()
