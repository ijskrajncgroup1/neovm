import string
import sys

####################################################################################################
#########################QUERIES FOR CHECKING VALIDITY OF TRANSITIONS###############################
####################################################################################################
# This functions check conditions on topology (to figure out if T1 transition needs to be undone)
def checkCondition(neo4j,ent1,ent2):
    q=string.Template("""MATCH (n:T1)-->(n1:$entity1)<--(n2:$entity2)
        WITH n1, collect(n2.name) AS lst
        WITH [n1.name, lst] AS lst
        WITH collect(lst) AS lst
        UNWIND lst AS x
        UNWIND lst AS y
        WITH x, y
        WHERE x[0]<>y[0]
        WITH size([item IN x[1] WHERE item IN y[1]]) AS commonCount
        WHERE commonCount>1
        RETURN count(1)/2 AS cnt""")
    return neo4j.run(q.substitute(entity1=ent1,entity2=ent2)).data()[0]['cnt']
# This function checks validity of the latesst topological transformation
def validity(neo4j):
    check=0
    if checkCondition(neo4j,"Edge","Vertex")>0:
        check+=1
    if checkCondition(neo4j,"Polygon","Edge")>0:
        check+=1
    if checkCondition(neo4j,"Cell","Polygon")>0:
        check+=1
    if check>0:
        sys.exit("Warning! Transformation not valid. Exiting...")
    return check
###############################################

####################################################################################################
############################USEFUL QUERY TEMPLATES##################################################
####################################################################################################
# This query ...
qSHARED=string.Template("""MATCH (n:T1)
    MATCH (n)-[:$n1]->($n1)
    MATCH (n)-[:$n2]->($n2)
    MATCH ($n1)<-[:IS_PART_OF]-($n:$label)-[:IS_PART_OF]->($n2)
    WITH n, $n LIMIT 1
    CREATE (n)-[:$n]->($n)
    """)
# This query ...
qFREE=string.Template("""MATCH (nf:Free)-[r:CONTAINS]->($n:$label)
    WITH r, $n LIMIT 1
    MATCH (n:T1)
    DELETE r
    CREATE (n)-[:$n]->($n)
    """)
# This query ...
qCELL=string.Template("""MATCH ($n1)<-[:$n1]-(n:T1)-[:$n2]->($n2)
    MATCH ($n1)-[:IS_PART_OF]->($n:Cell)<-[:IS_PART_OF]-($n2)
    WITH n, $n LIMIT 1
    CREATE (n)-[:$n]->($n)
    """)
# This query deletes a relationship
qDELETE=string.Template("""MATCH ($S)<-[:$S]-(n:T1)-[:$T]->($T)
    MATCH ($S)-[r:IS_PART_OF]->($T)           
    CREATE ($S)-[rr:WAS_PART_OF]->($T)
    SET rr.ori=r.ori            
    DELETE r
    """)
# This query creates a relationship
qCREATE=string.Template("""MATCH ($S)<-[:$S]-(n:T1)-[:$T]->($T)
    CREATE ($S)-[:IS_PART_OF]->($T)
    """)
# This query assigns orientation property to new vertex-->edge relationship
qSIGNv=string.Template("""MATCH ($S)<-[:$S]-(n:T1)-[:$T]->($T)
    MATCH ($S)-[r:IS_PART_OF]->($T)
    MATCH (n)-[:$V]->($V)
    MATCH ($V)-[rr:WAS_PART_OF]->($T)
    SET r.ori=rr.ori
    """)
# This query assigns orientation property to new edge-->polygon relationship
qSIGNe=string.Template("""MATCH ($S)<-[:$S]-(n:T1)-[:$T]->($T)
    MATCH ($V)<-[:$V]-(n)-[:$E]->($E)
    MATCH ($S)-[r:IS_PART_OF]->($T)
    MATCH ($V)-[rVS:IS_PART_OF]->($S)
    MATCH ($V)-[rVE:IS_PART_OF]->($E)
    MATCH ($E)-[rET:IS_PART_OF]->($T)
    SET r.ori=-rVS.ori*rVE.ori*rET.ori
    """)
# This query assigns specified orientation property to a new relationship
qSIGN=string.Template("""MATCH ($S)<-[:$S]-(n:T1)-[:$T]->($T)
    MATCH ($S)-[r:IS_PART_OF]->($T)
    SET r.ori=$sign
    """)
# Delete WAS_PART_OF relationships
qwpo="""MATCH (n)-[r:WAS_PART_OF]->(m)      
    DELETE r
    """
# This query finds edges using e7 and one of its adjacent polygons
qedge=string.Template("""
    MATCH ($e7)<-[:$e7]-(t1:T1)-[:$v1]->($v1)
    MATCH (t1)-[:$p]->($p)
    MATCH ($v1)-[:IS_PART_OF]->(e)-[:IS_PART_OF]->($p) WHERE NOT e=$e7
    CREATE (e)<-[:$et]-(t1)""")
# This query finds polygon, which is common among the given edges
qpolySHARED=string.Template("""MATCH ($e1)<-[:$e1]-(t1:T1)-[:$e2]->($e2)
    MATCH ($e1)-[:IS_PART_OF]->(p:Polygon)<-[:IS_PART_OF]-($e2)
    CREATE (t1)-[:$pt]->(p)""")
###############################################

####################################################################################################
#######################PATTERN-MATCHING & GRAPH-TRANSFORMATION QUERIES##############################
####################################################################################################
# ET
# This funciton performs pattern matching for ET transition
def patternMatching_ET(neo4j,EID:int):
    # e7
    q1=string.Template("""
    MATCH (n:T1)
    MATCH (e7:Edge {name:$EdgeID})
    CREATE (n)-[:e7]->(e7)
    """)
    # v1
    q2="""MATCH (n:T1)-[:e7]->(e7)<-[:IS_PART_OF]-(v1)
    CREATE (n)-[:v1]->(v1)
    """
    # v2
    q3="""MATCH (n:T1)-[r:v1]->(v1)
    WITH n, r, v1 LIMIT 1
    DELETE r
    CREATE (n)-[:v2]->(v1)
    """
    # p3
    q4="""MATCH (n:T1)-[:e7]->(e7)-[:IS_PART_OF]->(p:Polygon)
    CREATE (n)-[:p3]->(p)
    """
    # p2
    q5="""MATCH (n:T1)-[r:p3]->(p3)
    WITH n, r, p3 LIMIT 2
    DELETE r
    CREATE (n)-[:p2]->(p3)
    """
    # p1
    q6="""MATCH (n:T1)-[r:p2]->(p2)
    WITH n, r, p2 LIMIT 1
    DELETE r
    CREATE (n)-[:p1]->(p2)
    """
    q7=qCELL.substitute(n="c1",n1="p1",n2="p2") #c1
    q8=qCELL.substitute(n="c2",n1="p1",n2="p3") #c2
    q9=qCELL.substitute(n="c3",n1="p2",n2="p3") #c3
    # c4
    q10="""MATCH (n:T1)
    MATCH (n)-[:c1]->(c1)
    MATCH (n)-[:c2]->(c2)
    MATCH (n)-[:c3]->(c3)
    MATCH (n)-[:v1]->(v1)-[:IS_PART_OF]->(e:Edge)-[:IS_PART_OF]->(p:Polygon)-[:IS_PART_OF]->(c4:Cell)
    WHERE c4<>c1 AND c4<>c2 AND c4<>c3
    WITH n, c4 LIMIT 1
    CREATE (n)-[:c4]->(c4)
    """
    # c5
    q11="""MATCH (n:T1)
    MATCH (n)-[:c1]->(c1)
    MATCH (n)-[:c2]->(c2)
    MATCH (n)-[:c3]->(c3)
    MATCH (n)-[:v2]->(v2)-[:IS_PART_OF]->(e:Edge)-[:IS_PART_OF]->(p:Polygon)-[:IS_PART_OF]->(c5:Cell)
    WHERE c5<>c1 AND c5<>c2 AND c5<>c3
    WITH n, c5 LIMIT 1
    CREATE (n)-[:c5]->(c5)
    """
    q12=qedge.substitute(e7="e7", v1="v1", p="p1", et="e1") # e1
    q13=qedge.substitute(e7="e7", v1="v1", p="p2", et="e2") # e2
    q14=qedge.substitute(e7="e7", v1="v2", p="p1", et="e3") # e3
    q15=qedge.substitute(e7="e7", v1="v2", p="p2", et="e4") # e4
    q16=qedge.substitute(e7="e7", v1="v1", p="p3", et="e5") # e5
    q17=qedge.substitute(e7="e7", v1="v2", p="p3", et="e6") # e6
    q18=qpolySHARED.substitute(e1="e1", e2="e2", pt="p4") # p4
    q19=qpolySHARED.substitute(e1="e3", e2="e4", pt="p5") # p5
    q20=qpolySHARED.substitute(e1="e2", e2="e5", pt="p6") # p6
    q21=qpolySHARED.substitute(e1="e4", e2="e6", pt="p7") # p7
    q22=qpolySHARED.substitute(e1="e1", e2="e5", pt="p8") # p8
    q23=qpolySHARED.substitute(e1="e3", e2="e6", pt="p9") # p9
    q24=qFREE.substitute(n="v3",label="Vertex") #v3
    q25=qFREE.substitute(n="e8",label="Edge") #e8
    q26=qFREE.substitute(n="e9",label="Edge") #e9
    q27=qFREE.substitute(n="p10",label="Polygon") #p10
    # RUN QUERIES
    for q in [q1.substitute(EdgeID=str(EID)),q2,q3,q4,q5,q6,q7,q8,q9,q10,q11,q12,q13,q14,q15,q16,q17,q18,q19,q20,q21,q22,q23,q24,q25,q26,q27]:
        neo4j.run(q)
# This funciton performs graph transformation for ET transition
def graphTransformation_ET(neo4j):
    q1= qDELETE.substitute(S="v1",T="e2") # delete v1->e2
    q2= qDELETE.substitute(S="v1",T="e5") # delete v1->e5
    q3= qDELETE.substitute(S="v2",T="e3") # delete v2->e3 
    q4= qDELETE.substitute(S="v2",T="e6") # delete v2->e6 
    q5= qDELETE.substitute(S="e7",T="p1") # delete e7->p1
    q6= qDELETE.substitute(S="e7",T="p2") # delete e7->p2
    q7= qDELETE.substitute(S="e7",T="p3") # delete e7->p3
    q8= qCREATE.substitute(S="v1",T="e3") # create v1->e3
    q9= qCREATE.substitute(S="v1",T="e9") # create v1->e9
    q10= qCREATE.substitute(S="v2",T="e2") # create v2->e2
    q11= qCREATE.substitute(S="v2",T="e8") # create v2->e8
    q12= qCREATE.substitute(S="v3",T="e5") # create v3->e5
    q13= qCREATE.substitute(S="v3",T="e6") # create v3->e6
    q14= qCREATE.substitute(S="v3",T="e8") # create v3->e8
    q15= qCREATE.substitute(S="v3",T="e9") # create v3->e9
    q16= qCREATE.substitute(S="e7",T="p4") # create e7->p4
    q17= qCREATE.substitute(S="e7",T="p5") # create e7->p5
    q17b= qCREATE.substitute(S="e7",T="p10") # create e7->p10
    q18= qCREATE.substitute(S="e8",T="p6") # create e8->p6
    q19= qCREATE.substitute(S="e8",T="p7") # create e8->p7
    q20= qCREATE.substitute(S="e8",T="p10") # create e8->p10
    q21= qCREATE.substitute(S="e9",T="p8") # create e9->p8
    q22= qCREATE.substitute(S="e9",T="p9") # create e9->p9
    q23= qCREATE.substitute(S="e9",T="p10") # create e9->p10
    q24= qCREATE.substitute(S="p10",T="c4") # create p10->c4
    q25= qCREATE.substitute(S="p10",T="c5") # create p10->c5
    q26=qSIGNv.substitute(S="v1",T="e3",V="v2") # set orientations v1->e3
    q27=qSIGN.substitute(S="v1",T="e9",sign="-1") # set orientations v1->e9
    q28=qSIGNv.substitute(S="v2",T="e2",V="v1") # set orientations v2->e2
    q29=qSIGN.substitute(S="v2",T="e8",sign="1") # set orientations v2->e8
    q30=qSIGNv.substitute(S="v3",T="e5",V="v1") # set orientations v3->e5
    q31=qSIGNv.substitute(S="v3",T="e6",V="v2") # set orientations v3->e6
    q32=qSIGN.substitute(S="v3",T="e8",sign="-1") # set orientations v3->e8
    q33=qSIGN.substitute(S="v3",T="e9",sign="1") # set orientations v3->e9
    q34=qSIGNe.substitute(S="e7",T="p4",V="v2",E="e2") # set orientations e7->p4
    q35=qSIGNe.substitute(S="e7",T="p5",V="v1",E="e3") # set orientations e7->p5
    q36=qSIGNe.substitute(S="e8",T="p6",V="v2",E="e2") # set orientations e8->p6
    q37=qSIGNe.substitute(S="e8",T="p7",V="v3",E="e6") # set orientations e8->p7
    q38=qSIGNe.substitute(S="e9",T="p8",V="v3",E="e5") # set orientations e9->p8
    q39=qSIGNe.substitute(S="e9",T="p9",V="v1",E="e3") # set orientations e9->p9
    q40="""MATCH (n:T1)-[:v1]->(v1)
    MATCH (n)-[:e7]->(e7)
    MATCH (n)-[:p10]->(p10)
    MATCH (v1)-[r1:IS_PART_OF]->(e7)
    MATCH (e7)-[r:IS_PART_OF]->(p10)
    set r.ori=r1.ori
    """ # set orientations e7->p10
    q41=qSIGN.substitute(S="e8",T="p10",sign="1") # set orientations e8->p10
    q42=qSIGN.substitute(S="e9",T="p10",sign="1") # set orientations e9->p10
    q43="""MATCH (p4)<-[:p4]-(n:T1)-[:p10]->(p10)
    MATCH (n)-[:e7]->(e7)
    MATCH (n)-[:c4]->(c4)
    MATCH (n)-[:c5]->(c5)
    MATCH (e7)-[r74:IS_PART_OF]->(p4)
    MATCH (e7)-[r710:IS_PART_OF]->(p10)
    MATCH (p4)-[r44:IS_PART_OF]->(c4)
    MATCH (p10)-[r4:IS_PART_OF]->(c4)
    MATCH (p10)-[r5:IS_PART_OF]->(c5)
    SET r4.ori=-r710.ori*r74.ori*(r44.ori)
    SET r5.ori=-r4.ori
    """# set orientations p10->c4 and p10->c5
    # RUN QUERIES
    for q in [q1,q2,q3,q4,q5,q6,q7,q8,q9,q10,q11,q12,q13,q14,q15,q16,q17,q17b,q18,q19,q20,q21,q22,q23,q24,q25,q26,q27,q28,q29,q30,q31,q32,q33,q34,q35,q36,q37,q38,q39,q40,q41,q42,q43,qwpo]:
        neo4j.run(q)
# TE
# This funciton performs pattern matching for TE transition
def patternMatching_TE(neo4j,PID:int):
    # p10
    q1=string.Template("""MATCH (n:T1)
    MATCH (p10:Polygon{name:$PolygonID})
    CREATE (n)-[:p10]->(p10)
    """)
    # e7
    q2="""MATCH (n:T1)-[:p10]->(p10)<-[:IS_PART_OF]-(e7:Edge)
    WITH n, e7 LIMIT 1
    CREATE (n)-[:e7]->(e7)
    """
    # v1
    q3="""MATCH (n:T1)-[:e7]->(e7)<-[r:IS_PART_OF]-(v1)
    WHERE r.ori=1
    CREATE (n)-[:v1]->(v1)
    """
    # v2
    q4="""MATCH (n:T1)-[:e7]->(e7)<-[r:IS_PART_OF]-(v2)
    WHERE r.ori=-1
    CREATE (n)-[:v2]->(v2)
    """
    # v3
    q5="""MATCH (n:T1)-[:p10]->(p10)<-[:IS_PART_OF]-(e:Edge)<-[r:IS_PART_OF]-(v3)
    MATCH (v2)<-[:v2]-(n)-[:v1]->(v1)
    WHERE v3<>v1 AND v3<>v2
    WITH n, v3 LIMIT 1
    CREATE (n)-[:v3]->(v3)
    """
    # e8
    q6="""MATCH (e7)<-[:e7]-(n:T1)-[:p10]->(p10)
    MATCH (p10)<-[:IS_PART_OF]-(e8:Edge)<-[:IS_PART_OF]-(v2)<-[:v2]-(n)
    WHERE e8<>e7
    CREATE (n)-[:e8]->(e8)
    """
    # e9
    q7="""MATCH (n:T1)-[:p10]->(p10)<-[:IS_PART_OF]-(e9:Edge)
    MATCH (n)-[:e7]->(e7)
    MATCH (n)-[:e8]->(e8)
    WHERE e9<>e7 AND e9<>e8
    CREATE (n)-[:e9]->(e9)
    """
    # c4
    q8="""MATCH (n:T1)-[:p10]->(p10)-[:IS_PART_OF]->(c4:Cell)
    CREATE (n)-[:c4]->(c4)
    """
    # c5
    q9="""MATCH (n:T1)-[r:c4]->(c5:Cell)
    WITH n, r, c5 LIMIT 1
    DELETE r
    CREATE (n)-[:c5]->(c5)
    """
    # c1
    q10="""MATCH (n:T1)-[:e7]->(e7)
    MATCH (n)-[:c4]->(c4)
    MATCH (n)-[:c5]->(c5)
    MATCH (e7)-[:IS_PART_OF]->(p:Polygon)-[:IS_PART_OF]->(c1:Cell)
    WHERE c1<>c4 AND c1<>c5
    WITH DISTINCT n, c1
    CREATE (n)-[:c1]->(c1)
    """
    # c2
    q11="""MATCH (n:T1)-[:e9]->(e9)
    MATCH (n)-[:c4]->(c4)
    MATCH (n)-[:c5]->(c5)
    MATCH (e9)-[:IS_PART_OF]->(p:Polygon)-[:IS_PART_OF]->(c2:Cell)
    WHERE c2<>c4 AND c2<>c5
    WITH DISTINCT n, c2
    CREATE (n)-[:c2]->(c2)
    """
    # c3
    q12="""MATCH (n:T1)-[:e8]->(e8)
    MATCH (n)-[:c4]->(c4)
    MATCH (n)-[:c5]->(c5)
    MATCH (e8)-[:IS_PART_OF]->(p:Polygon)-[:IS_PART_OF]->(c3:Cell)
    WHERE c3<>c4 AND c3<>c5
    WITH DISTINCT n, c3
    CREATE (n)-[:c3]->(c3)
    """
    q13=qSHARED.substitute(n="p1",n1="c1",n2="c2",label="Polygon") #p1
    q14=qSHARED.substitute(n="p2",n1="c1",n2="c3",label="Polygon") #p2
    q15=qSHARED.substitute(n="p3",n1="c2",n2="c3",label="Polygon") #p3
    q16=qSHARED.substitute(n="p4",n1="c1",n2="c4",label="Polygon") #p4
    q17=qSHARED.substitute(n="p5",n1="c1",n2="c5",label="Polygon") #p5
    q18=qSHARED.substitute(n="p6",n1="c3",n2="c4",label="Polygon") #p6
    q19=qSHARED.substitute(n="p7",n1="c3",n2="c5",label="Polygon") #p7
    q20=qSHARED.substitute(n="p8",n1="c2",n2="c4",label="Polygon") #p8
    q21=qSHARED.substitute(n="p9",n1="c2",n2="c5",label="Polygon") #p9
    q22=qSHARED.substitute(n="e1",n1="p1",n2="p4",label="Edge") #e1
    q23=qSHARED.substitute(n="e2",n1="p2",n2="p4",label="Edge") #e2
    q24=qSHARED.substitute(n="e3",n1="p1",n2="p5",label="Edge") #e3
    q25=qSHARED.substitute(n="e4",n1="p2",n2="p5",label="Edge") #e4
    q26=qSHARED.substitute(n="e5",n1="p3",n2="p6",label="Edge") #e5
    q27=qSHARED.substitute(n="e6",n1="p3",n2="p7",label="Edge") #e6
    # RUN QUERIES
    for q in [q1.substitute(PolygonID=str(PID)),q2,q3,q4,q5,q6,q7,q8,q9,q10,q11,q12,q13,q14,q15,q16,q17,q18,q19,q20,q21,q22,q23,q24,q25,q26,q27]:
        neo4j.run(q)
# This funciton performs graph transformation for TE transition
def graphTransformation_TE(neo4j):  
    q1= qDELETE.substitute(S="v1",T="e3") # delete v1->e3
    q2= qDELETE.substitute(S="v1",T="e9") # delete v1->e9
    q3= qDELETE.substitute(S="v2",T="e2") # delete v2->e2
    q4= qDELETE.substitute(S="v2",T="e8") # delete v2->e8
    q5= qDELETE.substitute(S="v3",T="e5") # delete v3->e5
    q6= qDELETE.substitute(S="v3",T="e6") # delete v3->e6
    q7= qDELETE.substitute(S="v3",T="e8") # delete v3->e8
    q8= qDELETE.substitute(S="v3",T="e9") # delete v3->e9
    q9= qDELETE.substitute(S="e7",T="p4") # delete e7->p4
    q10= qDELETE.substitute(S="e7",T="p5") # delete e7->p5
    q11= qDELETE.substitute(S="e7",T="p10") # delete e7->p10
    q12= qDELETE.substitute(S="e8",T="p6") # delete e8->p6
    q13= qDELETE.substitute(S="e8",T="p7") # delete e8->p7
    q14= qDELETE.substitute(S="e8",T="p10") # delete e8->p10
    q15= qDELETE.substitute(S="e9",T="p8") # delete e9->p8
    q16= qDELETE.substitute(S="e9",T="p9") # delete e9->p9
    q17= qDELETE.substitute(S="e9",T="p10") # delete e9->p10
    q18= qDELETE.substitute(S="p10",T="c4") # delete p10->c4
    q19= qDELETE.substitute(S="p10",T="c5") # delete p10->c5
    q20= qCREATE.substitute(S="v1",T="e2") # create v1->e2
    q21= qCREATE.substitute(S="v1",T="e5") # create v1->e5
    q22= qCREATE.substitute(S="v2",T="e3") # create v2->e3
    q23= qCREATE.substitute(S="v2",T="e6") # create v2->e6
    q24= qCREATE.substitute(S="e7",T="p1") # create e7->p1
    q25= qCREATE.substitute(S="e7",T="p2") # create e7->p2
    q26= qCREATE.substitute(S="e7",T="p3") # create e7->p3
    q27=qSIGNv.substitute(S="v1",T="e2",V="v2") # set orientations v1->e2
    q28=qSIGNv.substitute(S="v1",T="e5",V="v3") # set orientations v1->e5
    q29=qSIGNv.substitute(S="v2",T="e3",V="v1") # set orientations v2->e3
    q30=qSIGNv.substitute(S="v2",T="e6",V="v3") # set orientations v2->e6
    q31=qSIGNe.substitute(S="e7",T="p1",V="v2",E="e3") # set orientations e7->p1
    q32=qSIGNe.substitute(S="e7",T="p2",V="v1",E="e2") # set orientations e7->p2
    q33=qSIGNe.substitute(S="e7",T="p3",V="v2",E="e6") # set orientations e7->p3
    # RUN QUERIES
    for q in [q1,q2,q3,q4,q5,q6,q7,q8,q9,q10,q11,q12,q13,q14,q15,q16,q17,q18,q19,q20,q21,q22,q23,q24,q25,q26,q27,q28,q29,q30,q31,q32,q33,qwpo]:
        neo4j.run(q)
###############################################
