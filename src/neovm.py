import math
import random
import sys
import queries as Q
import queriesT1 as T1
random.seed(2)
import os
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import numpy as np
from visualization_snapshot_3D import plot_3D_snap
from visualization_snapshot_2D import plot_2D_snap

class database:
    def __init__(self, neo4j, t):
        # essential variables
        self.file=''
        self.neo4j=neo4j
        # functions
        self.setup_DB(t)
    # This function sets up knowledge-graph database
    def setup_DB(self,t):
        script=t.generate_DB_setup_script()
        Q.setupDB(self.neo4j,script)  
    # This function creates a new Free vertex node
    def add_vertex(self, t):
        t.Nv+=1
        Q.new_node('Vertex',t.Nv-1,self.neo4j)
        vertex(t.Nv-1,[0,0,0],t)
    # This function creates a new Free edge node
    def add_edge(self, t):
        t.Ne+=1
        Q.new_node('Edge',t.Ne-1,self.neo4j)
        edge(t.Ne-1,[],t)           
    # This function creates a new Free polygon node
    def add_polygon(self, t):
        t.Np +=1
        Q.new_node('Polygon',t.Np-1,self.neo4j)
        polygon(t.Np-1,[],[],t)
    # This function creates free nodes in case there are not enough for an ET transition
    def add_nodes_ET(self,t):
        # vertex nodes
        if Q.count_free_nodes('Vertex',self.neo4j)<1:
            self.add_vertex(t)
        # edge nodes
        n=Q.count_free_nodes('Edge',self.neo4j)
        if n<2:
            for i in range(2-n):
                self.add_edge(t)
        # polygon nodes
        if Q.count_free_nodes('Polygon',self.neo4j)<1:
            self.add_polygon(t)
    
class vertex:
    def __init__(self, id, r, t):
        # essential variables
        self.id=id
        self.r=r
        self.exist=1
        # reset vertex properties
        self.reset_prop(t)
        # add it to list of vertices
        t.vertices.append(self)
    # This function resets vertex properties
    def reset_prop(self,t):
        self.F=[0,0,0]

class edge:
    def __init__(self, id, e, t):
        # essential variables
        self.id=id
        self.e=e
        self.exist=1
        # reset edge properties
        self.reset_prop(t)
        # add it to list of edges
        t.edges.append(self)
    # This function resets edge properties
    def reset_prop(self,t):
        self.length=0
        self.e_dl=0
        self.center=[0,0,0]
        self.clock=0
        self.lineTension=t.dg0+t.sig*random.normalvariate(0,1)
        if self.lineTension<0:
            self.lineTension=0
        if self.lineTension>2:
            self.lineTension=2
    # This function
    def edge_vertices_pbc(self,t):
        # vertex ids
        v1_id=self.e[0]
        v2_id=self.e[1]
        # pbc correct
        v1=t.torus_corrected_position(t.vertices[v1_id].r,t.vertices[v2_id].r) #v1
        v2=[t.vertices[v2_id].r[0],t.vertices[v2_id].r[1],t.vertices[v2_id].r[2]] #v2
        # return
        return [v1_id,v2_id,v1,v2]
    # This function calculates edge length
    def edge_length(self,t):
        # vertices
        [v1_id,v2_id,v1,v2]=self.edge_vertices_pbc(t)
        # length calculation
        dr=[0,0,0]
        for i in range(3):
            dr[i]=v2[i]-v1[i]
        return math.sqrt(dr[0]*dr[0]+dr[1]*dr[1]+dr[2]*dr[2])   
    # This function calculates forces due to line tension
    def force_lineTension(self,t):
        # vertices
        [v1_id,v2_id,v1,v2]=self.edge_vertices_pbc(t)
        # force calculation
        dr=[0,0,0]
        for i in range(3):
            dr[i]=v2[i]-v1[i]
        c0=-self.lineTension/(self.length+1e-8)
        for i in range(3):
            t.vertices[v1_id].F[i] += -c0*dr[i]
            t.vertices[v2_id].F[i] +=  c0*dr[i]
        return self.lineTension*self.length 
    # This function calculates edge midpoint
    def edge_center(self,t):
        # vertices
        [v1_id,v2_id,v1,v2]=self.edge_vertices_pbc(t)
        # midpoint calculation
        v_mid=[0,0,0]
        for i in range(3):
            v_mid[i]=0.5*(v1[i]+v2[i])
        # return pbc-corrected position
        return t.torus_on_position(v_mid)
    # This function corrects vertex positions upon an ET transition
    def correct_vertex_positions_ET(self, t, DB):
        # find v1, v2, v3
        v1v2v3=Q.vertices_v1_v2_v3(DB.neo4j)
        # find polygons p1, p2, and p3
        p1p2p3=Q.polygons_p1_p2_p3(DB.neo4j)
        # correct vertex positions
        v_corr=[]
        for i in range(t.dimen):
            # storing new coordinates in a 2D array
            v1_id=v1v2v3[0]
            v2_id=v1v2v3[1]
            # move v1 to mid and v2 to center
            t.vertices[v1_id].r=[self.center[0],self.center[1],self.center[2]]
            t.vertices[v2_id].r=[t.polygons[p1p2p3[i]].center[0],t.polygons[p1p2p3[i]].center[1],t.polygons[p1p2p3[i]].center[2]]
            # a vector from mid point (v1 now) to the center of the neighbour polygon (v2 now)
            v2corr=t.torus_corrected_position(t.vertices[v2_id].r, t.vertices[v1_id].r)
            dr=[0,0,0]
            for j in range(3):
                dr[j]=v2corr[j]-t.vertices[v1_id].r[j]
            len_temp=math.sqrt(dr[0]*dr[0]+dr[1]*dr[1]+dr[2]*dr[2])
            # New coordinates of v2
            for j in range(3):
                t.vertices[v2_id].r[j]=t.vertices[v1_id].r[j]+t.new_len*(dr[j]/len_temp)
            # taking care of periodic boundary condition
            v2=t.torus_on_position(t.vertices[v2_id].r)
            # return
            v_corr.append(v2)
        #assigning new cordinates of v1, v2, v3
        for i in range(t.dimen):
            t.vertices[v1v2v3[i]].r=v_corr[i]
    # This function performs ET transition
    def ET_transition(self,t,DB):
        # add new nodes if needed
        if (t.dimen==3):
            DB.add_nodes_ET(t)
        # pattern matching
        T1.patternMatching_ET(DB.neo4j,self.id)
        # graph transformations
        T1.graphTransformation_ET(DB.neo4j)
        # check if the transformation is valid
        T1.validity(DB.neo4j)
        # correct vertex positions
        self.correct_vertex_positions_ET(t,DB)
        # updating tissue
        t.update(DB,'ET')
        # counter
        t.ET_count+=1
        # release the helping nodes
        Q.releaseHelpingNodes(DB.neo4j)
                                  
class polygon:
    def __init__(self, id:int, loe:int, loo:int, t):
        # essential variables
        self.id=id
        self.loe=loe #list of edges
        self.loo=loo #list of edge orientations
        self.exist=1
        # reset polygon properties
        self.reset_lov(t)
        self.reset_prop(t)
        # add it to list of polygons
        t.polygons.append(self)  
    # This function resets polygon properties
    def reset_prop(self,t):
        self.center=[0,0,0]
        self.area=0
        self.p_dA=0
    # This function resets list of vertices
    def reset_lov(self,t):
        self.lov=[]
        # oriented list of vertices
        if len(self.loe)>0:
            if self.loo[0]==1:
                vi=t.edges[self.loe[0]].e[0]
                vf=t.edges[self.loe[0]].e[1]
            else:
                vi=t.edges[self.loe[0]].e[1]
                vf=t.edges[self.loe[0]].e[0]
            self.lov.append(vi)
            vprev=vf
            while vprev!=vi:
                for i in range(1,len(self.loe)):
                    if self.loo[i]==1:
                        v1=t.edges[self.loe[i]].e[0]
                        v2=t.edges[self.loe[i]].e[1]
                    else:
                        v1=t.edges[self.loe[i]].e[1]
                        v2=t.edges[self.loe[i]].e[0]
                    if v1==vprev:
                        self.lov.append(v1)
                        vprev=v2
                        break
    # This function calculates polygon center
    def polygon_center(self,t):
        self.reset_lov(t)
        # reference vertex
        vref=[t.vertices[self.lov[0]].r[0],t.vertices[self.lov[0]].r[1],t.vertices[self.lov[0]].r[2]]
        # calculate polygon center
        cent=[vref[0],vref[1],vref[2]]
        for i in range(1,len(self.lov)):
            v=t.torus_corrected_position(t.vertices[self.lov[i]].r,vref)
            for j in range(3):
                cent[j]+=v[j]
        return t.torus_on_position([cent[0]/(len(self.lov)*1.0),cent[1]/(len(self.lov)*1.0),cent[2]/(len(self.lov)*1.0)])
    # This function
    def triangle_vertices_pbc(self,j,vref,t):
        # vertex ids
        if (j==(len(self.lov)-1)):
            v1_id=self.lov[j]
            v2_id=self.lov[0]
        else:
            v1_id=self.lov[j]
            v2_id=self.lov[j+1]        
        # pbc correct
        v1=t.torus_corrected_position(t.vertices[v1_id].r, vref) #v1
        v2=t.torus_corrected_position(t.vertices[v2_id].r, vref) #v2
        #return
        return [v1_id,v2_id,v1,v2]
    # This function returns area of one triangular element
    def dA(self, j, vref, t):
        # vertices
        [v1_id,v2_id,v1,v2]=self.triangle_vertices_pbc(j,vref,t)
        # calculate area
        ax=v2[1]*v1[2]-v1[1]*v2[2]+v1[1]*vref[2]-vref[1]*v1[2]-v2[1]*vref[2]+vref[1]*v2[2]
        ay=v1[0]*v2[2]-v2[0]*v1[2]-v1[0]*vref[2]+vref[0]*v1[2]+v2[0]*vref[2]-vref[0]*v2[2]
        az=v2[0]*v1[1]-v1[0]*v2[1]+v1[0]*vref[1]-vref[0]*v1[1]-v2[0]*vref[1]+vref[0]*v2[1]
        # return
        return 0.5*math.sqrt(ax*ax + ay*ay + az*az)
    # This function returns polygon area
    def polygon_area(self,t):
        self.reset_lov(t)
        sum=0.0
        vref=[self.center[0],self.center[1],self.center[2]]
        for j in range(len(self.lov)):
            sum+=self.dA(j,vref,t)
        return sum
    # This function calculates force contribution of one triangular element due to surface tension
    def force_surfaceTensionP(self, j, vref, t):
        # vertices
        [v1_id,v2_id,v1,v2]=self.triangle_vertices_pbc(j,vref,t)
        # force calculation
        ax=v2[1]*v1[2]-v1[1]*v2[2]+v1[1]*vref[2]-vref[1]*v1[2]-v2[1]*vref[2]+vref[1]*v2[2]
        ay=v1[0]*v2[2]-v2[0]*v1[2]-v1[0]*vref[2]+vref[0]*v1[2]+v2[0]*vref[2]-vref[0]*v2[2]
        az=v2[0]*v1[1]-v1[0]*v2[1]+v1[0]*vref[1]-vref[0]*v1[1]-v2[0]*vref[1]+vref[0]*v2[1]
        fac_area=0.5*math.sqrt(ax*ax + ay*ay + az*az)
        # gradient
        deriv = 0.25*t.g0/(fac_area+1.0e-8)
        ms=1/(1.*len(self.lov))
        a=[0,0,0]
        a[0] = az*(v2[1] - vref[1]) - ay*(v2[2] - vref[2])
        a[1] = ax*(v2[2] - vref[2]) - az*(v2[0] - vref[0])
        a[2] = ay*(v2[0] - vref[0]) - ax*(v2[1] - vref[1])
        b=[0,0,0]
        b[0] = az*(v1[1] - vref[1]) - ay*(v1[2] - vref[2])
        b[1] = ax*(v1[2] - vref[2]) - az*(v1[0] - vref[0])
        b[2] = ay*(v1[0] - vref[0]) - ax*(v1[1] - vref[1])
        c=[0,0,0]
        for k in range(3):
            c[k]=deriv*ms*(-a[k]+b[k])
        # vertices update
        #v1
        for k in range(3):
            t.vertices[v1_id].F[k]+=deriv*a[k]
        #v2    
        for k in range(3):
            t.vertices[v2_id].F[k]+=-deriv*b[k]
        #p_vertices
        for vr in self.lov:
            for k in range(3):
                t.vertices[vr].F[k]+=c[k]
    # This function calculates forces due to surface tension of one polygon
    def force_surfaceTension(self, t):
        self.reset_lov(t)
        vref=[self.center[0],self.center[1],self.center[2]]
        for j in range(len(self.lov)):
            self.force_surfaceTensionP(j,vref,t)
        return t.g0*self.area
    # This function calculates the force contribution of one triangular element due to the term (A-A0)^2 in 2D
    def force_Area_P(self, j, vref, t):
        # vertices
        [v1_id,v2_id,v1,v2]=self.triangle_vertices_pbc(j,vref,t)
        # force calculation
        ax=v2[1]*v1[2]-v1[1]*v2[2]+v1[1]*vref[2]-vref[1]*v1[2]-v2[1]*vref[2]+vref[1]*v2[2]
        ay=v1[0]*v2[2]-v2[0]*v1[2]-v1[0]*vref[2]+vref[0]*v1[2]+v2[0]*vref[2]-vref[0]*v2[2]
        az=v2[0]*v1[1]-v1[0]*v2[1]+v1[0]*vref[1]-vref[0]*v1[1]-v2[0]*vref[1]+vref[0]*v2[1]
        fac_area=0.5*math.sqrt(ax*ax + ay*ay + az*az)
        # gradient
        deriv = 0.25*2*t.kA*(self.area-t.A0)/(fac_area+1.0e-8)
        ms=1/(1.*len(self.lov))
        a=[0,0,0]
        a[0] = az*(v2[1] - vref[1]) - ay*(v2[2] - vref[2])
        a[1] = ax*(v2[2] - vref[2]) - az*(v2[0] - vref[0])
        a[2] = ay*(v2[0] - vref[0]) - ax*(v2[1] - vref[1])
        b=[0,0,0]
        b[0] = az*(v1[1] - vref[1]) - ay*(v1[2] - vref[2])
        b[1] = ax*(v1[2] - vref[2]) - az*(v1[0] - vref[0])
        b[2] = ay*(v1[0] - vref[0]) - ax*(v1[1] - vref[1])
        c=[0,0,0]
        for k in range(3):
            c[k]=deriv*ms*(-a[k]+b[k])
        # vertices update
        #v1
        for k in range(3):
            t.vertices[v1_id].F[k]+=deriv*a[k]
        #v2
        for k in range(3):
            t.vertices[v2_id].F[k]+=-deriv*b[k]
        #p_vertices
        for vr in self.lov:
            for k in range(3):
                t.vertices[vr].F[k]+=c[k]         
    # This function calculates forces due to the term (A-A0)^2 in 2D
    def force_Area(self, t):
        self.reset_lov(t)
        vref=[self.center[0],self.center[1],self.center[2]]
        for j in range(len(self.lov)):
            self.force_Area_P(j,vref,t)
        return t.kA*(self.area-t.A0)**2   
    # This function returns volume element corresponding to one triangular element
    def dV(self, j, vref, t, chk):
        # vertices
        [v1_id,v2_id,v1,v2]=self.triangle_vertices_pbc(j,vref,t)
        if chk==1:
            vert3=self.polygon_center(t)
        else:
            vert3=[self.center[0],self.center[1],self.center[2]]
        v3=t.torus_corrected_position(vert3, vref)
        # return
        return (-v1[2]*v2[1]*v3[0] + v1[1]*v2[2]*v3[0] + v1[2]*v2[0]*v3[1] - v1[0]*v2[2]*v3[1] - v1[1]*v2[0]*v3[2] + v1[0]*v2[1]*v3[2])/6.
    # This function calculates forces contribution of one triangular element due to cell volume preservation
    def force_volumeSpringP(self,j,vref,ori,cell_vol,t):
        # vertices
        [v1_id,v2_id,v1,v2]=self.triangle_vertices_pbc(j,vref,t)     
        cent=[self.center[0],self.center[1],self.center[2]]
        v3=t.torus_corrected_position(cent, vref) #v3
        # gradient
        deriv = -2*ori*t.kV*(cell_vol-t.V0)
        ms=1/(6.*len(self.lov))
        #v1
        t.vertices[v1_id].F[0]+=deriv*(-v2[2]*v3[1] + v2[1]*v3[2])/6.
        t.vertices[v1_id].F[1]+=deriv*( v2[2]*v3[0] - v2[0]*v3[2])/6.
        t.vertices[v1_id].F[2]+=deriv*(-v2[1]*v3[0] + v2[0]*v3[1])/6.
        #v2
        t.vertices[v2_id].F[0]+=deriv*( v1[2]*v3[1] - v1[1]*v3[2])/6.
        t.vertices[v2_id].F[1]+=deriv*(-v1[2]*v3[0] + v1[0]*v3[2])/6.
        t.vertices[v2_id].F[2]+=deriv*( v1[1]*v3[0] - v1[0]*v3[1])/6.
        #p_vertices
        a=[0,0,0]
        a[0] = deriv * ms * (v1[1]*v2[2] - v2[1]*v1[2])
        a[1] = deriv * ms * (v2[0]*v1[2] - v1[0]*v2[2])
        a[2] = deriv * ms * (v1[0]*v2[1] - v2[0]*v1[1])
        for vr in self.lov:
            for k in range(3):
                t.vertices[vr].F[k]+=a[k]
    # This function corrects vertex positions upon a TE transition
    def correct_vertex_positions_TE(self, t, DB):
        # v1 & v2
        [v1_id,v2_id]=Q.vertices_v1_v2(DB.neo4j)
        # Mid point of the target triangle is assigned to the v1
        t.vertices[v1_id].r=[self.center[0],self.center[1],self.center[2]]
        # Central point of a neighbor cell is assigned as v2 vertex (later modified)
        c4=Q.find_node('c4',DB.neo4j)
        t.vertices[v2_id].r=[t.cells[c4].center[0],t.cells[c4].center[1],t.cells[c4].center[2]]
        # a vector from v1 to the center of the neighbour cell (v2 now)
        v2corr=t.torus_corrected_position(t.vertices[v2_id].r, t.vertices[v1_id].r)
        dr=[0,0,0]
        for j in range(3):
            dr[j]=v2corr[j]-t.vertices[v1_id].r[j]
        len_temp=math.sqrt(dr[0]*dr[0]+dr[1]*dr[1]+dr[2]*dr[2])
        # v1temp
        v1temp=[0,0,0]
        for j in range(3):
            v1temp[j]=t.vertices[v1_id].r[j]
        # New coordinates of v2
        for j in range(3):
            t.vertices[v1_id].r[j]=v1temp[j]-t.new_len*(dr[j]/len_temp)
            t.vertices[v2_id].r[j]=v1temp[j]+t.new_len*(dr[j]/len_temp)
        # taking care of periodic boundary condition
        v1=t.torus_on_position(t.vertices[v1_id].r)
        v2=t.torus_on_position(t.vertices[v2_id].r)
        for j in range(3):
            t.vertices[v1_id].r[j]=v1[j]
            t.vertices[v2_id].r[j]=v2[j]
    # This function performs TE transition
    def TE_transition(self,t,DB):
        # pattern matching
        T1.patternMatching_TE(DB.neo4j,self.id)
        # graph transformations
        T1.graphTransformation_TE(DB.neo4j)
        # check if the transformation is valid
        T1.validity(DB.neo4j)
        # correct new vertices position
        self.correct_vertex_positions_TE(t,DB)
        # updating tissue
        t.update(DB,'TE')
        # counter
        t.TE_count+=1
        # release the helping nodes
        Q.releaseHelpingNodes(DB.neo4j)            

class cell:
    def __init__(self, id:int, lop:int, loo:int, t):
        # essential variables
        self.id=id
        self.lop=lop #list of polygons
        self.loo=loo #list of polygon orientations
        self.exist=1
        # reset cell properties
        self.reset_lov(t)
        self.reset_prop()
        # add it to list of cells
        t.cells.append(self) 
    # This function resets cell properties
    def reset_prop(self):
        self.center=[0,0,0]
        self.volume=0
    # This function resets list of vertices
    def reset_lov(self,t):
        self.lov=[]
        if len(self.lop)>0:
            cell_vertices_all=[]
            for i in range(len(self.lop)):
                t.polygons[self.lop[i]].reset_lov(t)
                for j in range(len(t.polygons[self.lop[i]].lov)):
                    cell_vertices_all.append(t.polygons[self.lop[i]].lov[j])
            # keep only unique vertices
            self.lov = list(set(cell_vertices_all))
    # This function calculates cell volume
    def cell_volume(self,t,chk):
        volsum=0.0
        if chk==1:
            vref=self.cell_center(t)
        else:
            vref=[self.center[0],self.center[1],self.center[2]]
        for i in range(len(self.lop)):
            t.polygons[self.lop[i]].reset_lov(t)
            for j in range(len(t.polygons[self.lop[i]].lov)):
                dv=t.polygons[self.lop[i]].dV(j,vref,t,chk)
                volsum+=self.loo[i]*dv
        return volsum
    # This function calculates forces due to volume preservation
    def force_volumeSpring(self,t):
        vref=[self.center[0],self.center[1],self.center[2]]
        for i in range(len(self.lop)):
            t.polygons[self.lop[i]].reset_lov(t)
            for j in range(len(t.polygons[self.lop[i]].lov)):
                t.polygons[self.lop[i]].force_volumeSpringP(j,vref,self.loo[i],self.volume,t)
        return t.kV*(self.volume-t.V0)*(self.volume-t.V0)
    # This function calculates cell center
    def cell_center(self, t):
        self.reset_lov(t)
        vref=[t.vertices[self.lov[0]].r[0],t.vertices[self.lov[0]].r[1],t.vertices[self.lov[0]].r[2]]
        # calculate cell center
        cent=[vref[0],vref[1],vref[2]]
        for i in range(1,len(self.lov)):
            v=t.torus_corrected_position(t.vertices[self.lov[i]].r,vref)
            for j in range(3):
                cent[j]+=v[j]
        return t.torus_on_position([cent[0]/(len(self.lov)*1.0),cent[1]/(len(self.lov)*1.0),cent[2]/(len(self.lov)*1.0)])
            
class tissue:
    def __init__(self, vt3d:str):
        # essential variables
        self.vt3d=vt3d
        self.Lxy=[]
        self.vertices=[]
        self.edges=[]
        self.polygons=[]
        self.cells=[]
        # model parameters
        self.h=0.001 # default time step
        self.Tmax=100 # total simulation time
        self.lengthTH=0.01 # length threshold
        self.clockTH=0.02 # clock threshold
        self.new_len=0.001 # length of new edge
        self.dg0=0.1 # baseline line tension
        self.sig=0.25 # magnitude of fluctuations
        self.kM=1 # myosin turnover rate
        self.g0=1 # surface tension
        self.kV=100 # volume compressibility modulus
        self.V0=1 # preferred cell volume
        self.A0=1 # preferred polygon area
        self.kA=100 # area compressibility modulus
        self.outFreq=0.5 # output frequency
        #self.sig_ini=0.35 # for simulated annealing
        # properties
        self.reset_prop()
        # initialize
        self.setup_from_vt3d()
        # update properties
        #self.update_props()   
    # This function resets tissue properties
    def reset_prop(self):
        self.Time=0
        self.timeCount= 0 #0.001
        self.time_intrvl=900
        self.itrvl_count=0
        self.f14_frac_sum=0
        self.outFolder=""
        self.fileCount=0
        self.wT=0
        self.Nv=0
        self.Ne=0
        self.Np=0
        self.Nc=0
        self.ET_count=0
        self.TE_count=0
        self.dimen=0
    # This function sets up tissue from the input .vt3d file
    def setup_from_vt3d(self):
        self.Lxy=[0,0,0]
        with open(self.vt3d, "r") as f:
            # check for the number of elements
            list_VEPC=[int(x) for x in f.readline().split()]
            # assignment of dimension
            self.dimen=len(list_VEPC)-1
            # nr vertices, edges, polygons
            self.Nv=list_VEPC[0]
            self.Ne=list_VEPC[1]
            self.Np=list_VEPC[2]
            if self.dimen==3:
                # cells in 3D
                self.Nc=list_VEPC[3]
            f.readline()
            # box size
            list_VSIZE=[float(x) for x in f.readline().split()]
            self.Lxy[0]=list_VSIZE[0]
            self.Lxy[1]=list_VSIZE[1]
            if self.dimen==3:
                self.Lxy[2]=list_VSIZE[2]
            else:
                self.Lxy[2]=0 # assigning Lxy[0] = 0 in 2D samples
            f.readline()
            # vertices
            for count in range(self.Nv):
                List_COR=[float(x) for x in f.readline().split()]
                xx=List_COR[0]
                yy=List_COR[1]
                if self.dimen==3:
                    zz=List_COR[2]
                else:
                    zz=0 # assigning z = 0 in 2D samples
                vertex(count,[xx,yy,zz],self)
            f.readline()
            # edges
            for count in range(self.Ne):
                list_V1V2=[x for x in f.readline().split()]
                vv1=list_V1V2[0]
                vv2=list_V1V2[1]
                edge(count, [int(vv1)-1, int(vv2)-1], self)
            f.readline()
            # polygons
            for count in range(self.Np):
                b = f.readline().split()
                edges=[]
                orientations=[]
                for i in range(len(b)):
                    if int(b[i])!=0 and i>0:
                        if int(b[i])>0:
                            edges.append(int(b[i])-1)
                            orientations.append(1)
                        else:
                            edges.append(abs(int(b[i])+1))
                            orientations.append(-1)
                polygon(count, edges, orientations, self)
            f.readline()
            # cells
            for count in range(self.Nc):            
                b = f.readline().split()
                polygons=[]
                orientations=[]
                for i in range(1,len(b)):
                    if int(b[i])!=0:
                        if int(b[i])>0:
                            polygons.append(int(b[i])-1)
                            orientations.append(-1) #change here for Kelvin
#                            orientations.append(1) #changed due to Kelvin
                        else:
                            polygons.append(abs(int(b[i])+1))
                            orientations.append(1) #change here for Kelvin
#                            orientations.append(-1) #changed due to Kelvin
                cell(count, polygons, orientations, self)
        f.close()
    # This function generates a script to setup database
    def generate_DB_setup_script(self):
        # creating nodes
        DB_setup_script="CREATE  (n1:T1),\n        (n2:Free),\n"
        DB_setup_script+="        (t1:Tissue {name:0}),\n"
        n_count=0
        for c in self.cells:
            if (c.exist==1):
                DB_setup_script+="        (c"+str(n_count)+":Cell {name:"+str(n_count)+"}),\n"
                n_count +=1
        n_count=0
        for p in self.polygons:
            if (p.exist==1):
                DB_setup_script+="        (p"+str(n_count)+":Polygon {name:"+str(n_count)+"}),\n"
                n_count +=1
        n_count=0
        for e in self.edges:
            if (e.exist==1):
                DB_setup_script+="        (e"+str(n_count)+":Edge {name:"+str(n_count)+"}),\n"
                n_count +=1
        n_count=0
        for v in self.vertices:
            if (v.exist==1):
                DB_setup_script+="        (v"+str(n_count)+":Vertex {name:"+str(n_count)+"}),\n"
                n_count +=1        
        # creating relationships
        # for Vertex -> Edge
        for count in range(len(self.edges)):
            if(len(self.edges[count].e)>0):
                DB_setup_script+="        (v"+str(self.edges[count].e[0])+")-[:IS_PART_OF {ori:1}]->(e"+str(count)+"),\n"
                DB_setup_script+="        (v"+str(self.edges[count].e[1])+")-[:IS_PART_OF {ori:-1}]->(e"+str(count)+"),\n"
        # for Edge -> Polygon
        for count in range(len(self.polygons)):
            edges=self.polygons[count].loe
            orientations=self.polygons[count].loo
            if(len(edges)>0):
                for i in range(len(edges)):
                    edge=edges[i]
                    orientation=orientations[i]
                    if orientation>0:
                        if(count==(len(self.polygons)-1) and i== (len(edges)-1) and self.dimen!=3):
                            DB_setup_script+="        (e"+str(abs(edge))+")-[:IS_PART_OF {ori:1}]->(p"+str(count)+")"
                        else:
                            DB_setup_script+="        (e"+str(abs(edge))+")-[:IS_PART_OF {ori:1}]->(p"+str(count)+"),\n"
                        
                    else:
                        if(count==(len(self.polygons)-1) and i== (len(edges)-1) and self.dimen!=3):
                            DB_setup_script+="        (e"+str(abs(edge))+")-[:IS_PART_OF {ori:-1}]->(p"+str(count)+")"
                        else:
                            DB_setup_script+="        (e"+str(abs(edge))+")-[:IS_PART_OF {ori:-1}]->(p"+str(count)+"),\n"
        # for Polygon -> Cell
        for count in range(len(self.cells)):
            polygons=self.cells[count].lop
            orientations=self.cells[count].loo
            if(len(polygons)>0):
                for i in range(len(polygons)):
                    polygon=polygons[i]
                    orientation=orientations[i]
                    if orientation>0:
                        DB_setup_script+="        (p"+str(abs(polygon))+")-[:IS_PART_OF {ori:1}]->(c"+str(count)+"),\n"
                    else:
                        DB_setup_script+="        (p"+str(abs(polygon))+")-[:IS_PART_OF {ori:-1}]->(c"+str(count)+"),\n"
        # for Cell -> Tissue
        for count in range(len(self.cells)):
            if(count<self.Nc):
                if count<(self.Nc-1):
                    DB_setup_script+="        (c"+str(count)+")-[:IS_PART_OF]->(t1),\n"
                else:
                    DB_setup_script+="        (c"+str(count)+")-[:IS_PART_OF]->(t1)"
        return DB_setup_script
    # This function corrects position due to periodic boundary conditions
    def torus_on_position(self,v):
        dxdydz=[0,0,0]
        for i in range(3):
            if v[i]<0:
                dxdydz[i]=self.Lxy[i]
            elif v[i]>self.Lxy[i]:
                dxdydz[i]=-self.Lxy[i]
        return [v[0]+dxdydz[0],v[1]+dxdydz[1],v[2]+dxdydz[2]]
    # This function calculates displacement of one vertex vs another due to periodic boundary conditions
    def torus_corrected_position(self, v, vref):
        dxdydz=[0,0,0]
        for i in range(3):
            if abs(v[i]-vref[i])>0.5*self.Lxy[i]:
                if v[i]<vref[i]:
                    dxdydz[i]=self.Lxy[i]
                elif v[i]>vref[i]:
                    dxdydz[i]=-self.Lxy[i]
        return [v[0]+dxdydz[0],v[1]+dxdydz[1],v[2]+dxdydz[2]]
    # This function calculates forces on vertices
    def forces(self):
        # line energy
        wActive=0
        for e in self.edges:
            if e.exist==1 and e.lineTension!=0:
                wActive+=e.force_lineTension(self)
        # surface energy
        wA=0
        for p in self.polygons:
            if p.exist==1 and self.g0!=0:
                if self.dimen==3:
                    wA+=p.force_surfaceTension(self)
                else:
                    wA+=p.force_Area(self)
        # volume conservation
        wV=0
        for c in self.cells:
            if c.exist==1 and self.kV!=0 and self.dimen==3:
                wV+=c.force_volumeSpring(self)
        # total energy
        self.wT=wA+wV
        return [wActive,wA,wV]
    # This function updates objects' calculable properties
    def update_props(self, when):
        # edge lengths
        for e in self.edges:
            if e.exist==1:
                e.center=e.edge_center(self)
                elnew=e.edge_length(self)
                if when=='after':
                    e.e_dl=elnew-e.length
                    e.clock+=self.h
                e.length=elnew
        # polygon areas and centers
        aMIN=100
        for p in self.polygons:
            if p.exist==1:
                p.center=p.polygon_center(self)
                pAnew=p.polygon_area(self)
                if when=='after':
                    p.p_dA=pAnew-p.area
                p.area=pAnew
                if pAnew < aMIN:
                    aMIN=pAnew
        # cell volumes
        vMIN=100
        for c in self.cells:
            if c.exist==1 and self.dimen==3:
                c.center=c.cell_center(self)
                cVnew=c.cell_volume(self,0)
                if cVnew<vMIN:
                    vMIN=cVnew
                c.volume=cVnew
        # return
        if (self.dimen==3):
            return vMIN
        else:
            return aMIN
    # This function propagates tensions
    def propagate_tensions(self):
        for e in self.edges:
            if e.exist==1:
                e.lineTension += -self.h*self.kM*(e.lineTension-self.dg0) + math.sqrt(2*self.h*self.sig*self.sig*self.kM)*random.normalvariate(0,1)
                if e.lineTension<0:
                    e.lineTension=0
                if e.lineTension>2:
                    e.lineTension=2
                    
    # To find the fraction of 14 sided cells
    def f_14(self):
        tot_c=0
        f14_c=0
        for c in self.cells:
            if c.exist==1:
                tot_c +=1
                if len(c.lop)==14:
                    f14_c +=1
                    
        f14_frac=f14_c/tot_c
        return f14_frac
    
                    
    # This function propagates tissue from t to t+dt
    def solver(self):
        # update props
        vv=self.update_props('before')     
        # calculate forces
        for v in self.vertices:
            if v.exist==1:
                v.F=[0,0,0]
        [wActive,wA,wV]=self.forces()
        # propagate vertices
        drMAX=0
        for v in self.vertices:
            if v.exist==1:
                # displace vertices
                for i in range(3):
                    v.r[i]+=self.h*v.F[i]            
                # check max displacement
                dr=self.h*math.sqrt(v.F[0]*v.F[0]+v.F[1]*v.F[1]+v.F[2]*v.F[2])
                if dr>drMAX:
                    drMAX=dr
        # proapgate tensions
        self.propagate_tensions()
        # update properties
        vMIN=self.update_props('after')  
        # time
        self.Time += self.h
        # output
        if self.Time>=self.timeCount:
            with open(self.outFolder+"/basic_var.txt", 'a') as filebv:
                filebv.write(str(round(self.Time,6))+"\t"+str(self.wT)+"\t"+str(wA)+"\t"+str(wV)+"\t"+str(self.ET_count)+"\t"+str(self.TE_count)+"\t"+str(drMAX)+"\t"+str(vv)+"\t"+str(vMIN)+'\n')
            filebv.close()
            self.fileCount+=1
            # print a snapshot as .out_vt3d
            self.out_vt3d(self.outFolder,self.fileCount)
            # plot a snapshot as .pdf
            if(self.dimen !=3):
                plot_2D_snap(self.outFolder,self.fileCount)
            else:
                plot_3D_snap(self.outFolder,self.fileCount)
            self.timeCount+=self.outFreq #to print files at linear timescale
            #self.timeCount*=1.17645 # to print files at log timescale
#            if (self.dimen==3): # calculate only for 3D system
#                print("[t="+str(self.Time)+"] Intermediate result nr. "+str(self.fileCount)+" stored in "+self.outFolder+ "|| sigma: "+str(self.sig)+ ", f_14: "+str(self.f_14()))
#            else:
#                print("[t="+str(self.Time)+"] Intermediate result nr. "+str(self.fileCount)+" stored in "+self.outFolder+ "|| sigma: "+str(self.sig))
            print("[t="+str(self.Time)+"] Intermediate result nr. "+str(self.fileCount)+" stored in "+self.outFolder)
        # To find the average f_14 over last 100 time units
        if self.Time>=self.time_intrvl:
            self.itrvl_count +=1
            if (self.dimen==3): # calculate only for 3D system
                self.f14_frac_sum +=self.f_14()
            self.time_intrvl += 1.0
        
    # This function outputs a .vt3d file
    def out_vt3d(self, path:str, filenum:int):
        with open(path+'/out_'+str(filenum)+'.vt3d', 'w') as f3:
            # nr of elements
            list=[len(self.vertices),len(self.edges),len(self.polygons),len(self.cells)]             
            for i in range(len(list)):
                f3.write(str(list[i])+"\t")              
            f3.write("\n\n")
            # simulation box
            for i in range(len(self.Lxy)):
                f3.write(str(self.Lxy[i])+"\t")
            f3.write("\n\n")
            # vertices
            for v in self.vertices:
                if v.exist==1:
                    f3.write(str(v.exist)+"\t")
                    list=v.r
                    for i in range(len(list)):
                        f3.write(str(list[i])+"\t")
                    f3.write("\n")
                else:
                    f3.write(str(v.exist)+"\t"+str(0)+"\t"+str(0)+"\t"+str(0)+"\t")
                    f3.write("\n")
                    
            f3.write("\n")
            # edges
            for ed in self.edges:
                if ed.exist==1:
                    f3.write(str(ed.exist)+"\t")
                    list=ed.e
                    for i in range(len(list)):
                        f3.write(str(list[i]+1)+"\t")
                    f3.write("\n")
                    
                else:
                    f3.write(str(ed.exist)+"\t"+str(0)+"\t"+str(0)+"\t")
                    f3.write("\n")
            f3.write("\n")
            # polygons
            for p in self.polygons:
                if p.exist==1:
                    f3.write(str(p.exist)+"\t")
                    listP=p.loe
                    listO=p.loo
                    for i in range(len(listP)):
                        f3.write(str(listO[i]*(listP[i]+1))+"\t")
                    f3.write("\n")
                else:
                    f3.write(str(p.exist)+"\t"+str(0)+"\t"+str(0)+"\t"+str(0)+"\t")
                    f3.write("\n")
            f3.write("\n")
            # cells
            for c in self.cells:
                if c.exist==1:
                    listP=c.lop
                    listO=c.loo
                    for i in range(len(listP)):
                        f3.write(str(listO[i]*(listP[i]+1))+"\t")
                    f3.write("\n")
        f3.close()
    # This function goes over edges and performs topological transformations
    def topological_transitions(self,DB):
        for e in self.edges:
            if e.exist==1 and e.clock>self.clockTH:
                if e.length<self.lengthTH and e.e_dl<0:
                    # ET transition
                    if Q.count_edge_triangles(e.id,DB.neo4j)==0:
                        if Q.count_shared_polygons_c4c5(e.id,DB.neo4j)==0:
                            e.ET_transition(self,DB)
                    # TE transition
                    elif(Q.count_edge_triangles(e.id,DB.neo4j)!= 0 and self.dimen==3):
                        p=self.polygons[Q.edge_triangle(e.id,DB.neo4j)]
                        elist=Q.triangle_edges(p.id,DB.neo4j)
                        e1=self.edges[elist[0]]
                        e2=self.edges[elist[1]]
                        e3=self.edges[elist[2]]
                        if e1.clock>self.clockTH and e2.clock>self.clockTH and e3.clock>self.clockTH:
                            if e1.length<self.lengthTH and e2.length<self.lengthTH and e3.length<self.lengthTH:
                                if Q.polygon_triangles(p.id,DB.neo4j)==0:
                                    if p.p_dA<0:
                                        p.TE_transition(self,DB)
    # This function updates e, p, and c
    def update_edges_polygons_cells(self,DB):
        # Fetch data
        resE=Q.fetch_new_data("Edge", "Vertex", DB.neo4j)
        resP=Q.fetch_new_data("Polygon", "Edge", DB.neo4j)
        resC=Q.fetch_new_data("Cell", "Polygon", DB.neo4j)
        # edges
        for edg in resE:
            for i in range(len(edg[1])):
                if edg[1][i][1]==-1:
                    tail=edg[1][i][0]
                else:
                    head=edg[1][i][0]
            self.edges[edg[0]].e=[head,tail]
        # polygons
        for poly in resP:
            loe=[]
            loo=[]
            for i in range(len(poly[1])):
                loe.append(poly[1][i][0])
                loo.append(poly[1][i][1])
            self.polygons[poly[0]].loe=loe
            self.polygons[poly[0]].loo=loo
        # cells
        for cqs in resC:
            lop=[]
            loo=[]
            for i in range(len(cqs[1])):
                lop.append(cqs[1][i][0])
                loo.append(cqs[1][i][1])
            self.cells[cqs[0]].lop=lop
            self.cells[cqs[0]].loo=loo
    # This function updates the tissue upon a T1 transition
    def update(self,DB,trans):        
        # update edges, polygons, cells
        self.update_edges_polygons_cells(DB)    
        # new edges & polygons
        if trans=='ET':
            # find nodes
            e7=self.edges[Q.find_node('e7',DB.neo4j)]
            if(self.dimen==3):
                v3=self.vertices[Q.find_node('v3',DB.neo4j)]
                e8=self.edges[Q.find_node('e8',DB.neo4j)]
                e9=self.edges[Q.find_node('e9',DB.neo4j)]
                p10=self.polygons[Q.find_node('p10',DB.neo4j)]
                # .exist=1
                v3.exist=1
                e8.exist=1
                e9.exist=1
                p10.exist=1
                # reset properties
                v3.reset_prop(self)
                e8.reset_prop(self)
                e9.reset_prop(self)
                p10.reset_prop(self)
            e7.reset_prop(self)
        if trans=='TE':
            # find nodes
            v3=self.vertices[Q.find_node('v3',DB.neo4j)]
            e7=self.edges[Q.find_node('e7',DB.neo4j)]
            e8=self.edges[Q.find_node('e8',DB.neo4j)]
            e9=self.edges[Q.find_node('e9',DB.neo4j)]
            p10=self.polygons[Q.find_node('p10',DB.neo4j)]
            # .exist=0
            v3.exist=0
            e8.exist=0
            e9.exist=0
            p10.exist=0
            # reset new_edge
            e7.reset_prop(self)
            # free nodes
            Q.free_node('v3',DB.neo4j)
            Q.free_node('e8',DB.neo4j)
            Q.free_node('e9',DB.neo4j)
            Q.free_node('p10',DB.neo4j)
    # This function simulates the vertex model
    def simulate(self,DB,outFolder):
        # set output folder
        self.outFolder=outFolder
        # to delete already existing "basic_var.txt"
        if os.path.exists(outFolder+"/basic_var.txt"):
            os.unlink(outFolder+"/basic_var.txt")
        # set initial conditions for edge tensions
        for e in self.edges:
            if e.exist==1:
                e.reset_prop(self)
        # start a fresh "basic_var.txt"
        while (self.Time<self.Tmax):
            # updating sigma value for simulated annealing
            #self.sig=(self.Time/self.Tmax)*self.sig_ini
            #equation of motion
            self.solver()
            #T1 transition
            self.topological_transitions(DB)
        
        self.fileCount +=1 # increase the file number by 1
        print("The final filecount:", self.fileCount)
        # to print final vt3d file
        self.out_vt3d(self.outFolder,self.fileCount)
        # plot a snapshot as .pdf
        if(self.dimen !=3):
            plot_2D_snap(self.outFolder,self.fileCount)
        else:
            plot_3D_snap(self.outFolder,self.fileCount)
            #print("Total f_14 farction:", self.f14_frac_sum, "over:", self.itrvl_count)
        # to print total number of ET and TE transformations
        com_ET_TE=self.ET_count+self.TE_count
        print("Combined number of ET and TE:", com_ET_TE)
        
