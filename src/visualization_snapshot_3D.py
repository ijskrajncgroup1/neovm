import matplotlib.pyplot as plt
import numpy as np
import os
from mpl_toolkits.mplot3d.art3d import Poly3DCollection


def plot_3D_snap(path, file_number):
    file_name = f"out_{file_number}.vt3d"
    file_path = os.path.join(path, file_name)
    # Open the file
    with open(file_path, "r") as file:
        # Read the lines
        lines = file.readlines()
        # Extract values from the first line
        values_first_line = list(map(int, lines[0].split()))
        Nv, Ne, Np, Nc = values_first_line
        L=[]
        # Extract values from the third line
        values_third_line = list(map(float, lines[2].split()))
        Lx, Ly, Lz = values_third_line
        L.append(Lx)
        L.append(Ly)
        L.append(Lz)
        # Extract values from the fifth line
        values_fifth_line = list(map(float, lines[4].split()))
        flag, vx, vy, vz = values_fifth_line
        vertices=[]
        for i in range(4, 4 + Nv):
            list1=list(map(float, lines[i].split()))
            temp=[]
            temp.append(list1[1])
            temp.append(list1[2])
            temp.append(list1[3])
            vertices.append(temp)
        edges=[]
        for i in range(4 + Nv+1, 4 + Nv+Ne+1):
            list1=list(map(int, lines[i].split()))
            temp=[]
            temp.append(list1[1])
            temp.append(list1[2])
            edges.append(temp)
        polygons=[]
        for i in range(4 + Nv+Ne+1+1, 4 + Nv+Ne+1+1+Np):
            list1=list(map(int, lines[i].split()))
            temp=[]
            for i in range(len(list1)):
                temp.append(list1[i])
            polygons.append(temp)
        cells=[]
        for i in range(4 + Nv+Ne+1+1+Np+1, 4 + Nv+Ne+1+1+Np+1+Nc):
            list1=list(map(int, lines[i].split()))
            temp=[]
            for i in range(len(list1)):
                temp.append(list1[i])
            cells.append(temp)

    # Arranging edges of each polygon in anti-clockwise direction
    pp = []
    for j in range(len(polygons)):
        opolygons = [polygons[j][0]]  # Initialize opolygons[j]
        if polygons[j][0] == 1:
            p = polygons[j][1:]  # Drop the first element
            i = 0
            oeID = p[i]
            opolygons.append(oeID)
            if oeID > 0:
                v1, v2 = edges[abs(oeID) - 1][0], edges[abs(oeID) - 1][1]
            else:
                v1, v2 = edges[abs(oeID) - 1][1], edges[abs(oeID) - 1][0]
            for kk in range(len(p)-1):
                for k in range(len(p)):
                    if k != i:
                        oeID = p[k]
                        if oeID > 0:
                            V1N, V2N = edges[abs(oeID) - 1][0], edges[abs(oeID) - 1][1]
                        else:
                            V1N, V2N = edges[abs(oeID) - 1][1], edges[abs(oeID) - 1][0]
                        if v2 == V1N:
                            opolygons.append(oeID)
                            v2 = V2N
                            break
        pp.append(opolygons)
    polygons = pp

    allcells=[]
    for l in range(len(cells)):
        cell=[]
        pref=abs(cells[l][0])
        vref = vertices[edges[abs(polygons[pref-1][1]) - 1][0] - 1]
        for i in range(len(cells[l])):
            pID= abs(cells[l][i])
            polyCOOR=[]
            for j in range(1, len(polygons[pID-1])):
                oeID = polygons[pID-1][j]
                if oeID > 0:
                    V1 = vertices[edges[abs(oeID) - 1][0] - 1]
                    V2 = vertices[edges[abs(oeID) - 1][1] - 1]
                else:
                    V1 = vertices[edges[abs(oeID) - 1][1] - 1]
                    V2 = vertices[edges[abs(oeID) - 1][0] - 1]
                dxdydz = [0, 0, 0]
                for k in range(3):
                    if abs(V1[k] - vref[k]) > 0.5 * L[k]:
                        if V1[k] > vref[k]:
                            dxdydz[k] -= L[k]
                        else:
                            dxdydz[k] += L[k]
                polyCOOR.append([V1[m] + dxdydz[m] for m in range(3)])
            polyCOOR.append(polyCOOR[0])  # Closing the loop by adding the first point to the end
            cell.append(polyCOOR)
        allcells.append(cell)


    delta=[[0,0,0],[L[0],0,0], [0,L[1],0],[0,0,L[2]],[L[0],L[1],0],[L[0],0,L[2]],[0,L[1],L[2]],[L[0],L[1],L[2]],[-L[0],0,0],[0,-L[1],0],[0,0,-L[2]],[-L[0],-L[1],0],[-L[0],0,-L[2]],[0,-L[1],-L[2]],[-L[0],-L[1],-L[2]],[-L[0],L[1],0],[L[0],-L[1],0],[-L[0],0,L[2]],[L[0],0,-L[2]], [0,-L[1], L[2]], [0,L[1], -L[2]],[-L[0],L[1],L[2]],[L[0],-L[1],L[2]], [L[0],L[1],-L[2]],[-L[0],-L[1],L[2]], [-L[0],L[1],-L[2]], [L[0],-L[1],-L[2]]]

    cellToPlot=[]
    for cID in range(len(allcells)):
        cV=[]
        for pID in range(len(allcells[cID])):
            polygon=allcells[cID][pID]
            for vID in range(len(polygon)):
                cV.append(polygon[vID])
        for j in range(len(delta)):
            Flag=0
            for i in range(len(cV)):
                result = [x + y for x, y in zip(delta[j], cV[i])]
                if 0<result[0]<L[0] and 0<result[1]<L[1] and 0<result[2]<L[2]:
                    Flag=1
            if Flag==1:
                CELL=allcells[cID]
                NEW_CELL=[]
                for k in range(len(CELL)):
                    POLY=CELL[k]
                    New_poly=[]
                    for l in range(len(POLY)):
                        result1 = [x + y for x, y in zip(POLY[l], delta[j])]
                        New_poly.append(result1)
                    NEW_CELL.append(New_poly)
                cellToPlot.append(NEW_CELL)

    plt.clf() # clear the previous plot (if any)
    # Create a figure and subplot outside of the loop
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    # Initialize min and max values for each coordinate
    x_min, y_min, z_min = np.inf, np.inf, np.inf
    x_max, y_max, z_max = -np.inf, -np.inf, -np.inf
    ## Iterate through all cells
    for c in cellToPlot:
        for allpolycor in c:
            vertices = np.array(allpolycor)
            # Update min and max values for each coordinate
            x_min = 0
            y_min = 0
            z_min = 0
            x_max = L[0]
            y_max = L[1]
            z_max = L[2]
            # Define the polygon using Poly3DCollection
            polygon = Poly3DCollection([vertices])
            # Set the edge color to dark grey and the face color to light blue
            polygon.set_edgecolor((0.2, 0.2, 0.2, 0.2))  # Dark grey with transparency
            polygon.set_facecolor((0.8, 0.9, 1, 0.4))
            # Add the polygon to the plot
            ax.add_collection3d(polygon)
    # Set proper axis limits
    ax.set_xlim(-0.1*x_max, 1.1*x_max)
    ax.set_ylim(-0.1*y_max, 1.1*y_max)
    ax.set_zlim(-0.1*z_max, 1.1*z_max)
    # Remove grid, ticks, and tick labels
    ax.set_axis_off()
    # Add transparent green edges for the cube
    cube_vertices = [
        [x_min, y_min, z_min], [x_min, y_min, z_max],
        [x_min, y_max, z_min], [x_min, y_max, z_max],
        [x_max, y_min, z_min], [x_max, y_min, z_max],
        [x_max, y_max, z_min], [x_max, y_max, z_max]
    ]
    cube_edges = [
        [cube_vertices[0], cube_vertices[1]],
        [cube_vertices[0], cube_vertices[2]],
        [cube_vertices[0], cube_vertices[4]],
        [cube_vertices[1], cube_vertices[3]],
        [cube_vertices[1], cube_vertices[5]],
        [cube_vertices[2], cube_vertices[3]],
        [cube_vertices[2], cube_vertices[6]],
        [cube_vertices[3], cube_vertices[7]],
        [cube_vertices[4], cube_vertices[5]],
        [cube_vertices[4], cube_vertices[6]],
        [cube_vertices[5], cube_vertices[7]],
        [cube_vertices[6], cube_vertices[7]]
    ]
    for edge in cube_edges:
        ax.plot3D(*zip(*edge), color="g", alpha=0.8)
    # Set aspect ratio to create a proper cube
    max_range = np.array([x_max - x_min, y_max - y_min, z_max - z_min]).max()
    ax.set_box_aspect([max_range, max_range, max_range])
    out_file_name = f"out_{file_number}.pdf"
    out_file_path = os.path.join(path, out_file_name)
    # Save the plot as a PDF with minimal whitespace
    plt.savefig(out_file_path, format='pdf', bbox_inches='tight')
    # Close the plot window
    plt.close()
