import matplotlib.pyplot as plt
import numpy as np

# Load data from the .txt files
data_01 = np.loadtxt('output_sig_0.01/basic_var.txt')
data_01_s5 = np.loadtxt('output_sig_0.01_seed5/basic_var.txt')
data_01_s6 = np.loadtxt('output_sig_0.01_seed6/basic_var.txt')
data_17 = np.loadtxt('output_sig_0.17/basic_var.txt')
data_17_s3 = np.loadtxt('output_sig_0.17_seed3/basic_var.txt')
data_17_s4 = np.loadtxt('output_sig_0.17_seed4/basic_var.txt')
data_4 = np.loadtxt('output_sig_0.4/basic_var.txt')
data_4_s3 = np.loadtxt('output_sig_0.4_seed3/basic_var.txt')
data_4_s4 = np.loadtxt('output_sig_0.4_seed4/basic_var.txt')

# Extract the first column and 11th column for all datasets

column1_01 = data_01[:, 0]
column10_01 = data_01[:, 11]
column10_01_s5 = data_01_s5[:, 11]
column10_01_s6 = data_01_s6[:, 11]

column1_17 = data_17[:, 0]
column10_17 = data_17[:, 11]
column10_17_s3 = data_17_s3[:, 11]
column10_17_s4 = data_17_s4[:, 11]


column1_4 = data_4[:, 0]
column10_4 = data_4[:, 11]
column10_4_s3 = data_4_s3[:, 11]
column10_4_s4 = data_4_s4[:, 11]

# Calculate 1 - values from the 11th column for all datasets
column10_01_modified = 1 - column10_01
column10_01_s5_modified = 1 - column10_01_s5
column10_01_s6_modified = 1 - column10_01_s6

column10_17_modified = 1 - column10_17
column10_17_s3_modified = 1 - column10_17_s3
column10_17_s4_modified = 1 - column10_17_s4

column10_4_modified = 1 - column10_4
column10_4_s3_modified = 1 - column10_4_s3
column10_4_s4_modified = 1 - column10_4_s4

# Calculate the average
average_column10_01 = (column10_01_modified + column10_01_s5_modified + column10_01_s6_modified) / 3
average_column10_17 = (column10_17_modified + column10_17_s3_modified+column10_17_s4_modified) / 3
average_column10_4 = (column10_4_modified + column10_4_s3_modified + column10_4_s4_modified) / 3

# Enable LaTeX rendering for Matplotlib
plt.rcParams['font.family'] = 'Helvetica'

plt.figure(figsize=(1.65,1.65))

# Plot the second set of data
line1, = plt.plot(column1_01, average_column10_01, label=r'$\sigma=0.01$', color='red', markersize=3, linewidth=1)
line2, = plt.plot(column1_17, average_column10_17, label=r'$\sigma=0.17$', color='lime', markersize=3, linewidth=1)
line3, = plt.plot(column1_4, average_column10_4, label=r'$\sigma=0.4$', color='blue', markersize=3, linewidth=1)

plt.xlim(0.001, 1000)

# Increase tick size
plt.xticks(fontsize=8)
plt.yticks(fontsize=8)

plt.xlabel(r'$t$', fontsize=8)
plt.ylabel(r'$1-f_{14}$', fontsize=8)
plt.xscale('log')  # Set the x-axis to logarithmic scale
plt.legend(fontsize=6)

# Add a legend with adjusted marker size
plt.legend(handles=[line1, line2, line3], handlelength=0.5, fontsize=8)
# Save the plot as a PDF file with minimal white space
plt.savefig("plot_1-f14_from_voro.pdf", bbox_inches='tight', pad_inches=0.01, transparent=True)


plt.show()

