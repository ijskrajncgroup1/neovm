import numpy as np
import matplotlib.pyplot as plt

# Load the data from the file
data = np.loadtxt("data.txt")

# Split the data into three columns
col1 = data[:, 0]
col2 = data[:, 1]
col3 = data[:, 2]

# Scale the third column by its maximum value
col3_max = col3.max()
col3_scaled = col3 / col3_max

# Calculate (1 - col2 / 101)
col2_scaled = 1 - col2 / 101

# Create the figure and set the size
plt.figure(figsize=(1.65,1.65))

#plt.rcParams['text.usetex'] = True
plt.rcParams['font.family'] = 'Arial'

# Calculate the x-coordinates for the bars (centered around col1)
bar_width = 0.02  # Adjust the bar width as needed

# Plot col1 vs. (1 - col2 / 101) as a line plot
plt.plot(col1, col2_scaled, marker='o', linestyle='-', color='orangered', markersize=3, linewidth=1, label=r'$1-f_{14}$')

# Plot col1 vs. scaled col3 as a bar plot (centered around col1)
bar_x = col1  # Center the bars around col1
plt.bar(bar_x, col3_scaled, width=bar_width, color='grey', label=r'$n$')

plt.xlabel(r'$\sigma$', fontsize=8)  # Use r to indicate a raw string for LaTeX formatting
plt.ylabel('')
plt.xlim(-0.01, 0.51)

# Increase tick size
plt.xticks(fontsize=8)
plt.yticks(fontsize=8)

plt.legend(fontsize=8, loc='upper center', bbox_to_anchor=(0.45, 0.99))  # Increase legend font size

# Save the plot as a PDF file with minimal white space
plt.savefig("plot.pdf", bbox_inches='tight', pad_inches=0.01, transparent=True)

# Show or save the plot
plt.show()
# If you want to save the plot to a file, you can use plt.savefig("output.png") instead of plt.show()
