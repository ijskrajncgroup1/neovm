import numpy as np
import matplotlib.pyplot as plt

# Load the data from the file
data = np.loadtxt("data.txt")

# Split the data into three columns
col1 = data[:, 0]
col3 = data[:, 3]
col4 = data[:, 4]

# Create the figure and set the size
plt.figure(figsize=(1.65,1.65))

#plt.rcParams['text.usetex'] = True
plt.rcParams['font.family'] = 'Arial'

# Calculate the x-coordinates for the bars (centered around col1)
bar_width = 0.02  # Adjust the bar width as needed

# Plot col1 vs. (1 - col2 / 101) as a line plot with error bars
plt.errorbar(col1, col3, yerr=col4, marker='o', linestyle='', color='black', markersize=3, linewidth=1, capsize=2, ecolor='grey')

plt.xlabel(r'$\sigma$', fontsize=8)  # Use r to indicate a raw string for LaTeX formatting
plt.ylabel(r'$q$',fontsize=8)
plt.ylim(5.3, 5.7)
# Set the y-ticks explicitly
plt.yticks([5.3, 5.4, 5.5, 5.6, 5.7])
plt.xlim(-0.01, 0.51)

# Increase tick size
plt.xticks(fontsize=8)
plt.yticks(fontsize=8)

# Save the plot as a PDF file with minimal white space
plt.savefig("plot_q.pdf", bbox_inches='tight', pad_inches=0.01, transparent=True)

# Show or save the plot
plt.show()
# If you want to save the plot to a file, you can use plt.savefig("output.png") instead of plt.show()
