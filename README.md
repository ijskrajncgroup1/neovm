# neoVM

`neoVM` is a `Python` package for simulating tissue mechanics. It is based on the Graph vertex model, which describes a vertex model over a graph database. 

`neoVM` runs over a graph database set up in `Neo4j`. Cell rearrangements are implemented by graph transformations, which are executed by querying and transforming the graph data using pre-defined queries wtritten in `Cypher` query language. 

More details on the Graph vertex model and its implementation within the `neoVM` package are given in the following publication: 

- [1] T. Sarkar and M. Krajnc, Graph vertex model. arXiv: https://arxiv.org/abs/2309.04818

## Getting started

#### neoVM installation

To instal `neoVM` package, pull the repository, navigate to the parent directory and type the following command:

```
pip install .
```

Alternatively, `neoVM` can be installed directly from this online repository as

```
pip install git+https://neovm:neDz7jCS_vB49Y1Hjyu7@gitlab.com/ijskrajncgroup1/neovm.git
```
If `pip` does not work, the user can use `pip3` instead.   

#### Database setup in Neo4j

- Users are reqired to install `Neo4j` and set up an empty database prior to using `neoVM`. `Neo4j` is available [here](https://neo4j.com/product/developer-tools/)

We recommend installing `Neo4j Desktop` version, which is easy to use and allows convenient istallation of plugins such as `APOC`. `Neo4j` is an open source project. The Community Edition is fully open source, licensed and distributed under GPL v3. For more information click [here](https://neo4j.com/licensing/).

## Writing a neoVM Python script

After installing `neoVM` and setting up an empty graph database in `Neo4j`, `neoVM` can be used within user-written `Python` scripts, which need to follow the steps described below.

#### Include packages

Apart from `neoVM` package, users are required to import `py2neo` package, which is automatically installed together with `neoVM`.

```python
from neovm import tissue, database
from py2neo import Graph as graph
```

#### Define tissue and import the initial structure from .vt3d input file

The code below generates an object `t` of class `tissue` and sets up the initial tissue structure following the input data provided through a `.vt3d` file.

```python
t=tissue('<PATH_TO_INPUT_FOLDER>/initial.vt3d')
```

#### Setup database

The code below connects to a local graph database, setup in `Neo4j`, defines an object `db` of class `database`, and initializes a knowledge graph in `Neo4j` based on tissue `t`.

```python
neo=graph("bolt://localhost:7689", auth=("neo4j", "pass"))
db=database(neo,t)
```

#### Set up model parameters

The code below defines values of the model parameters. Those that are not specified are initialized to default values.

```python
t.h=0.001 # default time step
t.Tmax=20 # total simulation time
t.dg0=1 # 0.1 # baseline line tension
t.sig=0.25 # magnitude of tension fluctuations
t.kM=1 # myosin turnover rate
t.kV=100 # volume compressibility modulus
t.kA=100 # area compressibility modulus (only for 2D)
t.outFreq=0.5 #output frequency
```

#### Simulate

The code below simulates the tissue `t` over the graph database `db`.

```python
t.simulate(db,'<PATH_TO_OUTPUT_FOLDER>')
```

#### Visualization

Visualization is integrated within Python. During simulation, the code outputs .vt3d files as well as the corresponding python-generated images.

## Physical model

`neoVM` simulates a three-dimensional cell aggregate by integrating equations of motion for cell vertices. In dimensionless form,
```math
\frac{{\rm d}\boldsymbol r_i}{{\rm d}t}=-\sum_{\langle lm\rangle}\nabla_i A_{lm}-2\kappa_V\sum_{l}\left (V_l-V_0\right )\nabla_i V_l -\sum_{\langle lmn\rangle}\gamma_{lmn}(t)\nabla_i L_{lmn}
```

- Active force dipoles evolve in time according to the Ornstein-Uhlenbeck process. The effective line tension is defined as $\gamma_{lmn}(t)=\gamma_0+\Delta\gamma_{lmn}(t)$, where
```math
\dot\gamma_{lmn}(t)=-k_m\Delta\gamma_{lmn}(t)+\xi_{lmn}(t)
```

- Cells exchange their neighbors through topological transitions (i.e., edge-to-triangle and triangle-to-edge transitions).

More details on the model are given in [1].

## Additional useful functions

- `t.generate_DB_setup_script()`

This function prepares a data-import script for `Neo4j`, from object `t` of class `tissue`, previously set up from a `.vt3d` file. 
```python
script=t.generate_DB_setup_script()
print(script)
```

## Example

```python
from neovm import tissue, database
from py2neo import Graph as graph

# setup tissue
t=tissue('<PATH_TO_INPUT_FOLDER>/disordered_128.vt3d')

# connect to neo
neo=graph("bolt://localhost:7687", auth=("neo4j", "pass"))

# setup graph database
db=database(neo,t)

# set model parameters
t.h=0.001 # default time step
t.Tmax=20 # total simulation time
t.dg0=1 # 0.1 # baseline line tension
t.sig=0.25 # magnitude of tension fluctuations
t.kM=1 # myosin turnover rate
t.kV=100 # volume compressibility modulus
t.kA=100 # area compressibility modulus (only for 2D)
t.outFreq=0.5 #output frequency

#simulate
t.simulate(db,'<PATH_TO_OUTPUT_FOLDER>')
```

#### Expected output

<img src="https://gitlab.com/ijskrajncgroup1/neovm/-/raw/main/test/movie.gif" alt="Animated GIF" autoplay width="550">

## License

This software package is licensed under the GNU General Public License version 3.0 (GPL-3.0). For more details on the terms and conditions of this license, please see the 'COPYING' or 'LICENSE' file included with this software.

## Authors

- Tanmoy Sarkar, Jozef Stefan Institute, Ljubljana
- Matej Krajnc, Jozef Stefan Institute, Ljubljana

[Contact Us](mailto:matej.krajnc@ijs.si).
