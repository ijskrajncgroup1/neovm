from neovm import tissue, database
from py2neo import Graph as graph

t=tissue('<path>/disordered_128.vt3d')
neo=graph("bolt://localhost:7687", auth=("neo4j", "graphvertexmodel"))
db=database(neo,t)

t.h=0.001 # default time step
t.Tmax=20 # total simulation time
t.dg0=1 # baseline line tension
t.sig=0.25 # magnitude of tension fluctuations
t.kM=1 # myosin turnover rate
t.kV=100 # volume compressibility modulus
t.kA=100 # area compressibility modulus (only for 2D)
t.outFreq=0.5 #output frequency

t.simulate(db,'<path>')
